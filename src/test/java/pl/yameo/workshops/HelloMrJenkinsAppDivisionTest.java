package pl.yameo.workshops;

import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;
import java.util.Collection;

import static org.junit.Assert.assertEquals;

@RunWith(Parameterized.class)
public class HelloMrJenkinsAppDivisionTest {
    @Parameterized.Parameters
    public static Collection<Object[]> data() {
        return Arrays.asList(new Object[][]{
                {3, 2, 1.5},
                {20, 5, 4.0},
                {17, -111, -0.15},
                {-1, -1, -1.0}
        });
    }

    private HelloMrJenkinsApp app;
    private int a, b;
    private double expectedResult;

    public HelloMrJenkinsAppDivisionTest(int a, int b, double expectedResult) {
        this.a = a;
        this.b = b;
        this.expectedResult = expectedResult;

        app = new HelloMrJenkinsApp();
    }

    @Test
    public void when_divide_invoked_proper_result_returned() {
        assertEquals(app.divide(a, b), expectedResult, 0.01);
    }
}
