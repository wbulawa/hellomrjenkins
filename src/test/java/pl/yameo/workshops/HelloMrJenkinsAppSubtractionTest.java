package pl.yameo.workshops;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;
import java.util.Collection;

import static junit.framework.TestCase.assertEquals;

@RunWith(Parameterized.class)
public class HelloMrJenkinsAppSubtractionTest {
    @Parameterized.Parameters
    public static Collection<Object[]> data() {
        return Arrays.asList(new Object[][]{
                {3, 2, 1},
                {20, 5, 15},
                {1, -111, 112},
                {-1, -1, -1}
        });
    }

    private HelloMrJenkinsApp app;
    private int a, b;
    private long expectedResult;

    public HelloMrJenkinsAppSubtractionTest(int a, int b, long expectedResult) {
        this.a = a;
        this.b = b;
        this.expectedResult = expectedResult;

        app = new HelloMrJenkinsApp();
    }

    @Test
    public void when_subtract_invoked_proper_result_returned() {
        assertEquals(app.subtract(a, b), expectedResult);
    }
}
