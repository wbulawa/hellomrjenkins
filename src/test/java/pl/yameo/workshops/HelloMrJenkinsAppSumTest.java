package pl.yameo.workshops;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;
import java.util.Collection;

import static org.junit.Assert.assertEquals;

@RunWith(Parameterized.class)
public class HelloMrJenkinsAppSumTest {
    @Parameterized.Parameters
    public static Collection<Object[]> data() {
        return Arrays.asList(new Object[][]{
                {3, 2, 5},
                {20, 5, 25},
                {17, -111, -94},
                {-1, -1, -1}
        });
    }

    private HelloMrJenkinsApp app;
    private int a, b;
    private long expectedResult;

    public HelloMrJenkinsAppSumTest(int a, int b, long expectedResult) {
        this.a = a;
        this.b = b;
        this.expectedResult = expectedResult;

        app = new HelloMrJenkinsApp();
    }

    @Test
    public void when_sum_invoked_proper_result_returned() {
        assertEquals(app.sum(a, b), expectedResult);
    }
}
