package pl.yameo.workshops;

public class HelloMrJenkinsApp {
    public static void main( String[] args ) {
        new HelloMrJenkinsApp();
    }

    long sum(int a, int b) {
        return a + b;
    }

    long multiply(int a, int b) {
        return a * b;
    }

    long subtract(int a, int b) {
        return a - b;
    }

    //TODO
    double divide(int a, int b) { return (double) a / b; }
}
